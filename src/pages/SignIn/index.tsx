import React from 'react';
import { Container, Content, Background } from './styles';
import { FiLogIn } from 'react-icons/fi'
import logoimg from '../../assets/logo.svg'

import Input from '../../components/Input'
import Button from '../../components/Button'

const SignIn: React.FC = () => {
  return (
    <Container>
      <Content>
        <img src={logoimg} alt="" />

        <form>

          <h1>Faça seu logon</h1>

          <Input name='email' placeholder='E-mail' />
          <Input name='password' type="password" placeholder='Senha' />

          <Button type="submit">Entrar</Button>

          <a href="forgot-password">Esqueci minha senha</a>
        </form>

        <a href="">
          <FiLogIn />
          Criar conta
          </a>
      </Content>
      <Background />
    </Container>
  );
}

export default SignIn;
